import Cookies from 'js-cookie'

const TokenKey = 'vue_admin_template_token'
const UserInfoKey = 'user_info'

export function getToken() {
  return Cookies.get(TokenKey)
}

export function setToken(token) {
  return Cookies.set(TokenKey, token)
}

export function removeToken() {
  return Cookies.remove(TokenKey)
}

export function getUserInfo() {
  return Cookies.get(UserInfoKey)
}

export function setUserInfo(data) {
  return Cookies.set(UserInfoKey, data)
}

export function removeUserInfo() {
  return Cookies.remove(UserInfoKey)
}
