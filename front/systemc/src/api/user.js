import request from '@/utils/request'

/**
 * 登入
 * @param {} data
 * @returns
 */
export function login(data) {
  return request({
    url: '/admin/login',
    method: 'post',
    data
  })
}

/**
 *  根据token获取用户信息
 * @param {} token
 * @returns
 */
export function getInfo(token) {
  return request({
    url: '/saasUser/token',
    method: 'get',
    params: { token }
  })
}

/**
 * 退出
 */
export function logout() {
  return request({
    url: '/vue-admin-template/user/logout',
    method: 'post'
  })
}

/**
 * 添加管理员
 */
export function add() {
  return request({
    url: '/saasUser/add',
    method: 'post'
  })
}
