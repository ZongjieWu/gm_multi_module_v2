package com.wzj.test;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class UserMoney extends User{
    private BigDecimal money;
}
