package com.wzj.test;

import com.alibaba.druid.support.json.JSONUtils;

public class TestMain {
    public static void main(String[] args) {
        //测试1  类上定义泛型
//        UserSex sex=new UserSex();
//        sex.setSex(2);
//        CommonService<UserSex> commonService=new CommonService(sex);
//        UserSex userSex= commonService.process();
//        System.out.println(userSex.toString());
//        System.out.println(userSex.getToken());
//        System.out.println(userSex.getUserName());
//        System.out.println(userSex.getSex());

        //测试2 方法上定义泛型
        Common2Service common2Service=new Common2Service();
        UserSex sex=new UserSex();
        sex.setSex(2);
        sex = common2Service.ss(sex);
        System.out.println(sex.toString());
        System.out.println(sex.getToken());
        System.out.println(sex.getUserName());
        System.out.println(sex.getSex());

    }
}
