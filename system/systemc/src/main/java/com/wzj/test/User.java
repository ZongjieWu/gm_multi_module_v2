package com.wzj.test;

import lombok.Data;

@Data
public class User {
    private String userName;
    private String token;

}
