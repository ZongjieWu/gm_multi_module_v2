package com.wzj.systemc.permission.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wzj.systemc.permission.entity.SaasUserPermissionList;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author ZongjieWu
 * @since 2020-04-20
 */
@Repository
public interface SaasUserPermissionListMapper extends BaseMapper<SaasUserPermissionList> {
    List<String> getSaasUserPermission(Long saasUserId);
}
