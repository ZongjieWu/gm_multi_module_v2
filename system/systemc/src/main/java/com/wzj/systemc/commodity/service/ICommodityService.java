package com.wzj.systemc.commodity.service;

import com.wzj.common.object.result.Result;
import com.wzj.systemc.commodity.entity.Commodity;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ZongjieWu
 * @since 2021-03-31
 */
public interface ICommodityService extends IService<Commodity> {
}
