package com.wzj.systemc.permission.dto.saasuser;

import lombok.Data;


/**
 * 平台管理员添加请求数据
 */
@Data
public class SaasUserAddRequestVo {
    /**
     * 管理员角色id
     */
    private Long saasUserRoleId;

    /**
     * 头像
     */
    private String headImg;

    /**
     * 名称
     */
    private String name;

    /**
     * 电话
     */
    private String phone;

    /**
     * 密码
     */
    private String pwd;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 联系地址
     */
    private String addr;

    /**
     * 身份证
     */
    private String idCard;
}
