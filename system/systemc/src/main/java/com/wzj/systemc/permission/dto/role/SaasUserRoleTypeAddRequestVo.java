package com.wzj.systemc.permission.dto.role;

import lombok.Data;

/**
 * 平台管理员角色添加请求数据
 */
@Data
public class SaasUserRoleTypeAddRequestVo {
    /**
     * 角色名称
     */
    private String name;

    /**
     * 描述
     */
    private String descript;

}
