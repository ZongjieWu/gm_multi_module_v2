package com.wzj.systemc.permission.dto.permission;

import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * 权限修改请求数据
 */
@Data
public class SaasUserPermissionUpdateRequestVo extends SaasUserPermissionAddRequestVo{
    /**
     * 权限表id
     */
    @NotNull
    private Long id;
}
