package com.wzj.systemc.permission.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.wzj.systemc.permission.entity.SaasUserPermissionList;

import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ZongjieWu
 * @since 2020-04-20
 */
public interface ISaasUserPermissionListService extends IService<SaasUserPermissionList> {
    Map<String,Boolean> getSaasUserPermission(Long saasUserId);
}
