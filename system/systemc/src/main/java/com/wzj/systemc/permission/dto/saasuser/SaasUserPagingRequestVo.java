package com.wzj.systemc.permission.dto.saasuser;

import lombok.Data;

/**
 * 平台管理员获取分页请求数据
 */
@Data
public class SaasUserPagingRequestVo {
    /**
     * 关键词
     */
    private String keywords;
}
