package com.wzj.systemc.commodity.controller;


import com.wzj.common.object.result.Result;
import com.wzj.systemc.commodity.entity.Commodity;
import com.wzj.systemc.commodity.service.ICommodityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 *  商品相关
 * @author ZongjieWu
 * @since 2021-03-31
 */
@RestController
@RequestMapping("/commodity")
public class CommodityController {

    @Autowired
    private ICommodityService commodityService;


    /**
     * 根据用id获取信息xxxxx提交到分支3
     * @param id 商品id
     * @return
     */
    @GetMapping(value = "getById")
    public Result<Commodity> getById(Long id){
        Commodity commodity =commodityService.getById(id);
        return Result.returnSucessMsgData(commodity);
    }
}

