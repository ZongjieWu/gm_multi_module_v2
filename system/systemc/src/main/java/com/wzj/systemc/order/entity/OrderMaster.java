package com.wzj.systemc.order.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 订单表
 * </p>
 *
 * @author ZongjieWu
 * @since 2021-03-23
 */
@Data
@EqualsAndHashCode(callSuper = false)
//@Accessors(chain = true)
@TableName("t_order")
public class OrderMaster implements Serializable {

    private static final long serialVersionUID=1L;

    @TableId("id")
    private Long id;

    @TableField("user_id")
    private Long userId;

    @TableField("commodity_id")
    private Long commodityId;

    @TableField("num")
    private Integer num;

    @TableField("money")
    private BigDecimal money;

    @TableField("modify_datetime")
    private Date modifyDatetime;

    @TableField("create_datetime")
    private Date createDatetime;


}
