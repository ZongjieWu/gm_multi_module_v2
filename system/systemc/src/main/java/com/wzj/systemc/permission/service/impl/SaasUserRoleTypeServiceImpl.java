package com.wzj.systemc.permission.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wzj.systemc.permission.dao.SaasUserRoleTypeMapper;
import com.wzj.systemc.permission.entity.SaasUserRoleType;
import com.wzj.systemc.permission.service.ISaasUserRoleTypeService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ZongjieWu
 * @since 2020-04-20
 */
@Service
public class SaasUserRoleTypeServiceImpl extends ServiceImpl<SaasUserRoleTypeMapper, SaasUserRoleType> implements ISaasUserRoleTypeService {

}
