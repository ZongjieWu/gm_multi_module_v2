package com.wzj.systemc.permission.dto.saasuser;

import com.wzj.systemc.permission.entity.SaasUserPermissionList;
import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * 后台管理员基本信息实体响应数据
 */
@Data
public class SaasUserBaseInfoResponseVo {
    /**
     * 管理员
     */
    private Long id;

    /**
     * 管理员角色id
     */
    private Long saasUserRoleId;

    /**
     * 头像
     */
    private String headImg;

    /**
     * 名称
     */
    private String name;

    /**
     * 电话
     */
    private String phone;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 联系地址
     */
    private String addr;

    /**
     * 身份证
     */
    private String idCard;

    /**
     * 添加时间
     */
    private Date addTime;

    /**
     * 修改时间
     */
    private Date modifyTime;

    /**
     * 用户认证标识
     */
    private String token;

    /**
     * 状态
     */
    private Integer status;



    /**
     * #####################################################################################################
     * 以下存放非表映射字段
     * #####################################################################################################
     */

    /**
     * 管理员角色id
     */
    private String saasUserRoleName;


    private List<SaasUserPermissionList> saasUserPermissionList;

}
