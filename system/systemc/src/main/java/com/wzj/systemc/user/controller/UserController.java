package com.wzj.systemc.user.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.wzj.common.object.result.Result;
import com.wzj.systemc.user.entity.User;
import com.wzj.systemc.user.service.IUserService;
import com.wzj.test.CommonService;
import com.wzj.test.UserSex;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 *  用户相关接口
 * @author ZongjieWu
 * @since 2021-03-31
 */
@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired
    private IUserService userService;
    /**
     * 根据用户id获取用户信息
     * @param id 用户id
     * @return
     */
    @GetMapping(value = "getByUserId")
    public Result<List<User>> getByUserId(Long id){
        QueryWrapper<User> queryWrapper = new QueryWrapper<User>();
        queryWrapper.lambda().eq(User::getId, id);
        List<User> userList =userService.list(queryWrapper);
        return Result.returnSucessMsgData(userList);
    }


    /**
     * 根据用户id获取用户信息
     * @return
     */
    @GetMapping(value = "gtest")
    public Result<UserSex> gtest(){
        UserSex sex=new UserSex();
        sex.setSex(2);
        CommonService<UserSex> commonService=new CommonService(sex);
        UserSex userSex= commonService.process();
        System.out.println(userSex.toString());
        System.out.println(userSex.getToken());
        System.out.println(userSex.getUserName());
        System.out.println(userSex.getSex());
        return Result.returnSucessMsgData(userSex);
    }
}

