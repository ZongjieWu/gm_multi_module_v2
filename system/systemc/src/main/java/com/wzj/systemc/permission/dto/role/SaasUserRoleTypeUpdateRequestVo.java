package com.wzj.systemc.permission.dto.role;

import lombok.Data;


/**
 * 平台管理员角色添加请求数据
 */
@Data
public class SaasUserRoleTypeUpdateRequestVo extends SaasUserRoleTypeAddRequestVo{
    /**
     * 角色表id
     */
    private Long id;


}
