package com.wzj.systemc.permission.dto.saasuser;

import lombok.Data;

/**
 * 平台管理员添加请求数据
 */
@Data
public class SaasUserUpdateRequestVo extends SaasUserAddRequestVo{
    /**
     * 平台管理员的表id
     */
    private Long id;
}
