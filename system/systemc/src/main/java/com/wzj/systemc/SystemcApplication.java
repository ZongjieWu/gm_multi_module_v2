package com.wzj.systemc;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Repository;

@MapperScan(basePackages = {"com.wzj"}, annotationClass = Repository.class)
@ComponentScan(basePackages={"com.wzj"})
@SpringBootApplication
public class SystemcApplication {

	public static void main(String[] args) {
		SpringApplication.run(SystemcApplication.class, args);
	}

}
