package com.wzj.systemc.permission.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wzj.systemc.permission.dao.SaasUserPermissionListMapper;
import com.wzj.systemc.permission.entity.SaasUserPermissionList;
import com.wzj.systemc.permission.service.ISaasUserPermissionListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ZongjieWu
 * @since 2020-04-20
 */
@Service
public class SaasUserPermissionListServiceImpl extends ServiceImpl<SaasUserPermissionListMapper, SaasUserPermissionList> implements ISaasUserPermissionListService {

    @Autowired
    private SaasUserPermissionListMapper saasUserPermissionListMapper;
    @Override
    public Map<String,Boolean> getSaasUserPermission(Long saasUserId) {
        List<String> permissionStringList= saasUserPermissionListMapper.getSaasUserPermission(saasUserId);
        Map<String,Boolean> permissonMap=new HashMap<>();
        permissionStringList.forEach(item->{
            permissonMap.put(item,true);
        });
        return permissonMap;
    }
}
