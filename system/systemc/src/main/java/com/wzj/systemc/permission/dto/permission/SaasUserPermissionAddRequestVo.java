package com.wzj.systemc.permission.dto.permission;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * 权限添加请求数据
 */
@Data
public class SaasUserPermissionAddRequestVo {

    /**
     * 父级权限id
     */
    @NotNull
    private Long parentId;

    /**
     * 名称
     */
    @NotBlank
    private String name;

    /**
     * 描述
     */
    @NotBlank
    private String descript;

    /**
     * 请求路径
     */
    private String requestPath;


    /**
     * 图标路径
     */
    private String iconPath;

    /**
     * 排序
     */
    private Integer sort;
}
