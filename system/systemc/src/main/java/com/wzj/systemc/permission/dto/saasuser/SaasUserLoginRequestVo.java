package com.wzj.systemc.permission.dto.saasuser;

import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * 平台管理员登入请求数据
 */
@Data
public class SaasUserLoginRequestVo {
    /**
     * 电话号码
     */
    @NotBlank
    private String phone;
    /**
     * 密码
     */
    @NotBlank
    private String pwd;
}
