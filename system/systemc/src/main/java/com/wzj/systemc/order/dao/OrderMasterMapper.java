package com.wzj.systemc.order.dao;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wzj.systemc.order.dto.OrderMasterDataDTO;
import com.wzj.systemc.order.entity.OrderMaster;
import org.springframework.stereotype.Repository;


/**
 * <p>
 * 订单表 Mapper 接口
 * </p>
 *
 * @author ZongjieWu
 * @since 2021-03-23
 */
@Repository
public interface OrderMasterMapper extends BaseMapper<OrderMaster> {

    /**
     * 根据id查询详情
     * @param id
     * @return
     */
    OrderMasterDataDTO detailById(Long id);
}
