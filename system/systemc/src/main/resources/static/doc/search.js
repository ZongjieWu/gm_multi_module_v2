let api = [];
api.push({
    alias: 'CommodityController',
    order: '1',
    link: '商品相关',
    desc: '商品相关',
    list: []
})
api[0].list.push({
    order: '1',
    desc: '根据用id获取信息xxxxx提交到分支3',
});
api.push({
    alias: 'OrderMasterController',
    order: '2',
    link: '订单相关接口',
    desc: '订单相关接口',
    list: []
})
api[1].list.push({
    order: '1',
    desc: '根据用户id获取订单',
});
api[1].list.push({
    order: '2',
    desc: '根据id获取订单详情',
});
api.push({
    alias: 'SaasUserController',
    order: '3',
    link: '2.平台管理员,添加、修改、删除、查询等接口',
    desc: '2.平台管理员,添加、修改、删除、查询等接口',
    list: []
})
api[2].list.push({
    order: '1',
    desc: '',
});
api[2].list.push({
    order: '2',
    desc: '管理员添加',
});
api[2].list.push({
    order: '3',
    desc: '管理员修改',
});
api[2].list.push({
    order: '4',
    desc: '分页查询管理员',
});
api.push({
    alias: 'SaasUserLoginController',
    order: '4',
    link: '1.平台管理员',
    desc: '1.平台管理员',
    list: []
})
api[3].list.push({
    order: '1',
    desc: '登入',
});
api.push({
    alias: 'SaasUserPermissionListController',
    order: '5',
    link: '5.平台管理员权限,添加、修改、删除、查询等接口',
    desc: '5.平台管理员权限,添加、修改、删除、查询等接口',
    list: []
})
api[4].list.push({
    order: '1',
    desc: '添加权限',
});
api[4].list.push({
    order: '2',
    desc: '删除权限',
});
api[4].list.push({
    order: '3',
    desc: '修改权限',
});
api[4].list.push({
    order: '4',
    desc: '',
});
api[4].list.push({
    order: '5',
    desc: '分页查询管理员权限',
});
api[4].list.push({
    order: '6',
    desc: '',
});
api.push({
    alias: 'SaasUserRolePermissionController',
    order: '6',
    link: '4.平台角色权限控制器',
    desc: '4.平台角色权限控制器',
    list: []
})
api[5].list.push({
    order: '1',
    desc: '',
});
api[5].list.push({
    order: '2',
    desc: '',
});
api[5].list.push({
    order: '3',
    desc: '',
});
api[5].list.push({
    order: '4',
    desc: '',
});
api.push({
    alias: 'SaasUserRoleTypeController',
    order: '7',
    link: '3.平台管理员角色,添加、修改、删除、查询等接口',
    desc: '3.平台管理员角色,添加、修改、删除、查询等接口',
    list: []
})
api[6].list.push({
    order: '1',
    desc: '平台管理员角色添加',
});
api[6].list.push({
    order: '2',
    desc: '平台管理员角色的删除',
});
api[6].list.push({
    order: '3',
    desc: '平台管理员角色修改',
});
api[6].list.push({
    order: '4',
    desc: '获取角色列表',
});
api[6].list.push({
    order: '5',
    desc: '',
});
api[6].list.push({
    order: '6',
    desc: '平台管理员角色权限的修改',
});
api.push({
    alias: 'UserController',
    order: '8',
    link: '用户相关接口',
    desc: '用户相关接口',
    list: []
})
api[7].list.push({
    order: '1',
    desc: '根据用户id获取用户信息',
});
api.push({
    alias: 'dict',
    order: '9',
    link: 'dict_list',
    desc: '数据字典',
    list: []
})
document.onkeydown = keyDownSearch;
function keyDownSearch(e) {
    const theEvent = e;
    const code = theEvent.keyCode || theEvent.which || theEvent.charCode;
    if (code == 13) {
        const search = document.getElementById('search');
        const searchValue = search.value;
        let searchArr = [];
        for (let i = 0; i < api.length; i++) {
            let apiData = api[i];
            const desc = apiData.desc;
            if (desc.indexOf(searchValue) > -1) {
                searchArr.push({
                    order: apiData.order,
                    desc: apiData.desc,
                    link: apiData.link,
                    list: apiData.list
                });
            } else {
                let methodList = apiData.list || [];
                let methodListTemp = [];
                for (let j = 0; j < methodList.length; j++) {
                    const methodData = methodList[j];
                    const methodDesc = methodData.desc;
                    if (methodDesc.indexOf(searchValue) > -1) {
                        methodListTemp.push(methodData);
                        break;
                    }
                }
                if (methodListTemp.length > 0) {
                    const data = {
                        order: apiData.order,
                        desc: apiData.desc,
                        link: apiData.link,
                        list: methodListTemp
                    };
                    searchArr.push(data);
                }
            }
        }
        let html;
        if (searchValue == '') {
            const liClass = "";
            const display = "display: none";
            html = buildAccordion(api,liClass,display);
            document.getElementById('accordion').innerHTML = html;
        } else {
            const liClass = "open";
            const display = "display: block";
            html = buildAccordion(searchArr,liClass,display);
            document.getElementById('accordion').innerHTML = html;
        }
        const Accordion = function (el, multiple) {
            this.el = el || {};
            this.multiple = multiple || false;
            const links = this.el.find('.dd');
            links.on('click', {el: this.el, multiple: this.multiple}, this.dropdown);
        };
        Accordion.prototype.dropdown = function (e) {
            const $el = e.data.el;
            $this = $(this), $next = $this.next();
            $next.slideToggle();
            $this.parent().toggleClass('open');
            if (!e.data.multiple) {
                $el.find('.submenu').not($next).slideUp("20").parent().removeClass('open');
            }
        };
        new Accordion($('#accordion'), false);
    }
}

function buildAccordion(apiData, liClass, display) {
    let html = "";
    let doc;
    if (apiData.length > 0) {
        for (let j = 0; j < apiData.length; j++) {
            html += '<li class="'+liClass+'">';
            html += '<a class="dd" href="#_' + apiData[j].link + '">' + apiData[j].order + '.&nbsp;' + apiData[j].desc + '</a>';
            html += '<ul class="sectlevel2" style="'+display+'">';
            doc = apiData[j].list;
            for (let m = 0; m < doc.length; m++) {
                html += '<li><a href="#_' + apiData[j].order + '_' + doc[m].order + '_' + doc[m].desc + '">' + apiData[j].order + '.' + doc[m].order + '.&nbsp;' + doc[m].desc + '</a> </li>';
            }
            html += '</ul>';
            html += '</li>';
        }
    }
    return html;
}