package com.wzj.systemb.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wzj.systemb.dao.CommodityMapper;
import com.wzj.systemb.entity.Commodity;
import com.wzj.systemb.service.ICommodityService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 商品表 服务实现类
 * </p>
 *
 * @author ZongjieWu
 * @since 2021-03-23
 */
@Service
public class CommodityServiceImpl extends ServiceImpl<CommodityMapper, Commodity> implements ICommodityService {

}
