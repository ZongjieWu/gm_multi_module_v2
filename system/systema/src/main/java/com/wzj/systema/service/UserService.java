package com.wzj.systema.service;

import com.wzj.systema.entity.User;

/**
 * @author Zongjie Wu
 * @date 2021/3/22 16:50
 */
public interface UserService {
    /**
     * 根据id查找用户
     * @param id
     * @return
     */
    User getById(Long id);
}
